class category_info{
    category_id:number=0;
    category_name:string="";
    category_image:string="";
};


import Express,{Request,Response} from "express";
const category_cN = require("./connection");
// const category_express = require("express");
const category_rout = Express.Router();
category_rout.use(Express.json())

category_rout.get("/get", (req:Request,res:Response) => {
     const qry = "select * from category";
     category_cN.query(qry, (err:any, result:any) => {
         if (err) {
            return res.status(400).send(err.sqlMessage)
         }

         else {

             const category_detail_list:category_info[]=[]

             for(const i of Object.keys(result))
             {
                const category_detail:category_info=
                {
                    category_id:result[i].category_id,
                    category_name:result[i].category_name,
                    category_image:result[i].category_image
                }
                category_detail_list.push(category_detail)


             }

             return res.status(200).send(category_detail_list)
         }
     }

     )
 })


 category_rout.get("/get/:category_id", (req:Request,res:Response) => {



    const qry = `select * from category where category_id=? `
    category_cN.query(qry,req.params.category_id, (err:any, result:any) => {


        if (err) {
            return res.status(400).send(err.sqlMessage)
        }
        if (result.length==0)

        {
            return res.status(400).send("please enter valid category_id")
        }

        const category_detail:category_info=
        {
            category_id:result[0].category_id,
            category_name:result[0].category_name,
            category_image:result[0].category_image
        }

        return res.status(200).send(category_detail)
            

    })

})




category_rout.post("/post", (req:Request,res:Response) => {

    const keyList:[string,string]=["category_name","category_image"]
    const default_key:(string)[]=[]
    for (const item of Object.keys(req.body))
    {
        if(keyList.includes(item))
        {
            continue
        }
        else{
            default_key.push(item)
        }
    }


    if(default_key.length==0)
    {
        const data = req.body;
        const qry:string = "insert into category(category_name,category_image) values (?,?)";
        category_cN.query(qry, [data.category_name, data.category_image], (err:any, result:any) => {
    
            if (err) {
                if(err.sqlMessage.slice(0,9)=="Duplicate")
                {
                    return res.status(409).send("Duplicate Entry")
                }
                return res.status(400).send("Syntax error")
            }
            return res.status(200).send("Data Entry is successfull")
            
        }
    
        )


    }
    else{
        res.status(422).send({"not exist":default_key})
    }


})


category_rout.put("/put/:category_id", (req:Request,res:Response) => {  
    
    
    // category_id,category_name,category_description
    

    const qry = `select * from category`
    category_cN.query(qry, (err:any, result:any) => {
        if (err) {
            return res.status(400).send(err.sqlMessage)
        }
        else {
            
            if(result.length==0)
            {
                res.status(422).send("please enter valid category_id")
            }
            else{
                const keyList:[string,string]=["category_name","category_image"]
                const default_key:(string)[]=[]
                for (const item of Object.keys(req.body))
                {
                    if(keyList.includes(item))
                    {
                        continue
                    }
                    default_key.push(item)
                }

            
                
                if(default_key.length==0){
                   const data = req.body;
                    const  qry = "update category set  category_name=?,category_image=? where category_id=?";
                    category_cN.query(qry, [data.category_name, data.category_image, req.params.category_id], (err:any, result:any) => {
                
                        if (err) {
                            return res.status(400).send(err.sqlMessage)
                        }
                        return res.status(200).send("Data will be updated")
                        
                    }
                
                    )
            
                }
                else{
                    res.status(422).send({"not exist":default_key})
                }
            }
            }


        })


})




category_rout.delete("/delete/:category_id", (req:Request,res:Response) => {
    const qry = "select * from category where category_id=?";
    category_cN.query(qry,req.params.category_id, (err:any, result:any) => {
        if (err) {
            return res.status(400).send(err.sqlMessage)
        }

        if(result.length==0)
        {
            return res.status(422).send("this category id not present in database")
        }
        else{ const qry = "delete from category where category_id=?";
        category_cN.query(qry, req.params.category_id, (err:any, result:any) => {
            if (err) {
                return res.status(400).send(err.sqlMessage)
            }
            return res.send("Data  deleted")
            
        })}


        
    })
})





module.exports = category_rout;