class product_info{
    product_id:number=0;
    product_name:string="";
    product_image:string="";
    product_price:number=0;
    product_discount:number=0;
    product_discount_type:string="";
    product_discription:string="";
    category_id:number=0;
    category_name:string="";
    product_total_amount_after_discount?:number=0;


};
import Express,{Request,Response} from "express";
const product_cN = require("./connection");


const product_rout = Express.Router();
product_rout.use(Express.json())




product_rout.get("/get", (req:Request,res:Response) => {
    const qry:string = `select * from product as p 
    inner join category as  c on p.category_id=c.category_id
    `;
    product_cN.query(qry, (err:any, result:any) => {
        if (err) {
            return res.status(400).send(err.sqlMessage)
        }

        const product_detail_list:product_info[] = []
        for (const i of result)
        {
            const product_detail:product_info=
            {
                "product_id": i.product_id,
                "product_name": i.product_name,
                "product_image": i.product_image,
                "product_price": i.product_price,
                "product_discount": i.product_discount,
                "product_discount_type": i.product_discount_type,
                "product_discription": i.product_discription,
                "category_id": i.category_id,
                "category_name": i.category_name
            }
            if (i.product_discount_type == "percentage") {
                product_detail["product_total_amount_after_discount"]= i.product_price - (i.product_price * (i.product_discount / 100))
                product_detail_list.push(product_detail)
            }
            else{
                product_detail["product_total_amount_after_discount"]= i.product_price - i.product_discount
                product_detail_list.push(product_detail)
            }

        }

        return res.send(product_detail_list)
        
    }

    )
})





product_rout.get("/get/:product_id", (req:Request,res:Response) => {



    const qry = `select * from product as p 
    inner join category as  c on p.category_id=c.category_id where product_id=?;
    `;
    product_cN.query(qry, req.params.product_id, (err:any, result:any) => {
        if (err) {
            return res.status(400).send(err.sqlMessage)
        }

        if (result.length == 0) {
            return res.status(400).send("this id not present in database")
        }

        const product_detail:product_info=
        {
            "product_id": result[0].product_id,
            "product_name": result[0].product_name,
            "product_image": result[0].product_image,
            "product_price": result[0].product_price,
            "product_discount": result[0].product_discount,
            "product_discount_type": result[0].product_discount_type,
            "product_discription": result[0].product_discription,   
            "category_id": result[0].category_id,
            "category_name": result[0].category_name
        }

        if (result[0].product_discount_type == "percentage") {
            product_detail["product_total_amount_after_discount"] = result[0].product_price-(result[0].product_price*(result[0].product_discount/100))

        }
        else  {
            product_detail["product_total_amount_after_discount"] = result[0].product_price - result[0].product_discount

        }
        
       return res.send(product_detail)


    }

    )
})







product_rout.post("/post", (req:Request,res:Response) => {  //product_name,product_image,product_price,product_discount_type,product_discription,categories
    // data.product_image=LOAD_FILE('/path/to/your/image.jpg')
    //?,LOAD_FILE(?),?,?,?,?
    const data = req.body;
    const keyList:(string)[]=["product_name","product_image","product_price","product_discount","product_discount_type","product_discription","category_id"]
    const default_key:(string)[]=[]
    if(typeof data.product_price=="string" || typeof data.product_discount=="string" || typeof data.category_id=="string")
    {
        return res.status(400).send({error:"invalid syntax"})
    }

    for (const item of Object.keys(data))
    {
        if(keyList.includes(item))
        {
            continue
        }
        default_key.push(item)
        
    }

    if (default_key.length == 0) {
        const qry = `INSERT INTO product(product_name,product_image,product_price,product_discount,product_discount_type,product_discription,category_id)
        values(?,?,?,?,?,?,?)`;
        product_cN.query(qry, [data.product_name, data.product_image, data.product_price, data.product_discount, data.product_discount_type, data.product_discription, data.category_id], (err:any, result:any) => {

            if (err) {
                return res.status(400).send(err.sqlMessage)
            }
            return res.status(200).send("Data Entry is successfull")
            
        }

        )
    }
    else {
        return res.status(422).send({ "not exist": default_key })
    }


})


product_rout.put("/put/:product_id", (req:Request,res:Response) => {
    // product_id,product_name,product_image,product_price,product_discount,product_discount_type,product_discription,categories_id



    const qry = `select * from product where product_id=?`
    product_cN.query(qry,req.params.product_id, (err:any, result:any) => {
        if (err) {
            return res.status(400).send(err.sqlMessage)
        }
        else {

            if(result.length==0)
            {
                res.status(400).send("this product id not presend in data base")
            }

            else{
                const data = req.body;
                const keyList:(string)[]=["product_name","product_image","product_price","product_discount","product_discount_type","product_discription","category_id"]
                const default_key:(string)[]=[]
                if(typeof data.product_price=="string" || typeof data.product_discount=="string" || typeof data.category_id=="string")
                {
                    return res.status(400).send({error:"invalid syntax"})
                }
                for (const item of Object.keys(data))
                {
                    if(keyList.includes(item))
                    {
                        continue
                    }
                    default_key.push(item)
            
                }
                if (default_key.length == 0) {
                    const qry = "update product set  product_name=?,product_image=?,product_price=?,product_discount=?,product_discount_type=?,product_discription=?,category_id=? where product_id=?";
                    product_cN.query(qry, [data.product_name, data.product_image, data.product_price, data.product_discount, data.product_discount_type, data.product_discription, data.category_id, req.params.product_id], (err:any, result:any) => {

                        if (err) {
                            return res.status(400).send(err.sqlMessage)
                        }

                        else {
                           return  res.status(200).send("data will be updated")
                        }
                    }

                    )
                }
                else {
                    res.status(422).send({ "not exist": default_key })
                }





            }
        }
    })















})

product_rout.delete("/delete/:product_id", (req:any, res:any) => {



    const qry = "select * from product where product_id=?";
    product_cN.query(qry, req.params.product_id,(err:any, result:any) => {
        if (err) {
            return res.status(400).send(err.sqlMessage)
        }
        else {
            if(result.length==0)
            {
                res.status(422).send("product_id not present id database")
            }
            else{const qry = "delete from product where product_id=?";
            product_cN.query(qry, req.params.product_id, (err:any, result:any) => {
                if (err) {
                    return res.status(400).send(err.sqlMessage)
                }
                else {
                    res.send("data deleted")
                }
            })}
        }
    })
})






module.exports = product_rout;