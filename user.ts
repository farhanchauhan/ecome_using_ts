class user_info{
    user_id:number=0;
    user_name:string="";
    email:string="";
    mobile_number:number=0;
}

import Express,{Request,Response} from "express";
const usercN = require("./connection");
//const Express=require("express")
const userrout = Express.Router();
userrout.use(Express.json())
userrout.get("/get", (req:Request,res:Response) => {
    const qry:string = "select * from user";
    usercN.query(qry, (err:any, result:any) => {

        if (err) {
            
            return res.status(400).send({error:err.sqlMessage})
        }
        const user_detail_list:user_info[]= []
        for (const item of result)
        {
            const user_detail =
            {
                "user_id": item.user_id,
                "user_name": item.user_name,
                "email": item.email,
                "mobile_number": item.mobile_number

            }
            user_detail_list.push(user_detail)
        }

        res.status(200).send(user_detail_list)
        
    }

    )
})

userrout.get("/get/:user_id", (req:Request,res:Response) => {

   const  qry = `select * from user where user_id=?`
    usercN.query(qry,req.params.user_id, (err:any, result:any) => {
        if (err) {
            return res.status(400).send({error:err.sqlMessage})
        }
        if (result.length==0)
        {
            return res.status(400).send({error:"this user id not exist in database"})
        }
        const user_detail:user_info =
        {
            "user_id": result[0].user_id,
            "user_name": result[0].user_name,
            "email": result[0].email,
            "mobile_number": result[0].mobile_number

        }
        return res.send(user_detail)

        
})
})




userrout.post("/post", (req:Request,res:Response) => {  // user_name,email,mobile_number
    const data = req.body;
    const keyList:(string)[] = ["user_name", "email", "mobile_number"]
    const default_key:(string)[] = []
    if(typeof data.mobile_number=="string")
    {
        return res.status(400).send({error:"invalid syntax"})
    }

    for (const item of Object.keys(data))
    {
        if(keyList.includes(item))
        {
            continue
        }
        default_key.push(item)
        
    }

    if (default_key.length == 0) {

        const email_sampleRegEx: RegExp = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g;
        const mobile_number_sampleRegEx:RegExp=/^(\d{3})[- ]?(\d{3})[- ]?(\d{4})$/;

        if (email_sampleRegEx.test(data.email) == true && mobile_number_sampleRegEx.test(data.mobile_number) == true) {
      
            const qry = "insert into user(user_name,email,mobile_number) values (?,?,?)";
            const lst = [data.user_name, data.email, data.mobile_number]
            usercN.query(qry, lst, (err:any, result:any) => {

                if (err) {
                    if(err.sqlMessage.slice(0,9)=="Duplicate")
                    {
                        return res.status(409).send({error:"Duplicate Entry"})
                    }
                    return res.status(400).send({error:"Syntax error"})
                }
                return res.status(200).send({message:"Data Entry is successfull"})
                }
                )
        }

        else{
        return res.status(422).send({error:"please enter valid email and mobile number"})
        }
     }
    else{
        return res.status(422).send({not_exist: default_key })
    } 
}
)


userrout.put("/put/:user_id", (req:Request,res:Response) => {  // user_id ,user_name ,email ,mobile_number

   const  data = req.body;
    const keyList:(string)[] = ["user_name", "email", "mobile_number"]
    const default_key:(string)[] = []
    if(typeof data.mobile_number=="string")
    {
        return res.status(400).send({error:"invalid syntax"})
    }


    for (const item of Object.keys(data))
    {
        if(keyList.includes(item))
        {
            continue
        }
        default_key.push(item)
        
    }

    if (default_key.length == 0) {

        const email_sampleRegEx: RegExp = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g;
        const mobile_number_sampleRegEx:RegExp=/^(\d{3})[- ]?(\d{3})[- ]?(\d{4})$/;

        if (email_sampleRegEx.test(data.email) == true && mobile_number_sampleRegEx.test(data.mobile_number) == true)  {
            


            const qry:string = "update user set user_name=?,email=?,mobile_number=? where user_id=?";
            const lst = [data.user_name, data.email, data.mobile_number,req.params.user_id]
            usercN.query(qry, lst,(err:any, result:any) => {

                if (err) {
                    return res.status(400).send({error:err.sqlMessage})
                }
                return res.status(200).send({message:"Data will be updated"})
                
                
                
            }

            )
        }
        else{return res.status(422).send({error:"please enter valid email and mobile number"})}
        
        

    }
    else{
        return res.status(422).send({not_exist: default_key })
    }


})


userrout.delete("/delete/:user_id", (req:Request,res:Response) => {
   const  qry = "select * from user where user_id=?";
    usercN.query(qry,req.params.user_id, (err:any, result:any) => {
        if (err) {
            return res.status(400).send({error:err.sqlMessage})
        }
        
        if(result.length==0)
        {
            return res.status(422).send({error:"this user_id not present is database"})
        }
 
        const qry = "delete from user where user_id=?";
        usercN.query(qry, req.params.user_id, (err:any, result:any) => {
            if (err) {
                return res.status(400).send({error:err.sqlMessage})
            }
            return  res.send({message:"Data  deleted"})
            
        })


        
    })
})
//ss   s
module.exports=userrout;