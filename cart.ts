class cart_info{
    product_name:string="";
    product_image:string="";
    product_price:number=0;
    product_discount:number=0;
    product_discount_type:string="";
    product_discription:string="";
    quantity:number=0;
    total_amount?:number=0;
};

import Express,{Request,Response} from "express";
const cart_cN = require("./connection");
// const cart_express = require("express");
const cart_rout = Express.Router();
cart_rout.use(Express.json())

cart_rout.get("/get/:user_id", (req:Request,res:Response) => {

   const qry:string = `select u.user_id,u.user_name,p.product_name,p.product_image,p.product_price,p.product_discount,p.product_discount_type,p.product_discription,c.quantity from ecommerce.cart as c
    left join ecommerce.user as u on c.user_id=u.user_id
    left join ecommerce.product as p on p.product_id =c.product_id
    where u.user_id=?`;
    cart_cN.query(qry, req.params.user_id, (err:any, result:any) => {

        if (err) {
            return res.status(400).send(err.sqlMessage)
        }

        if (result.length == 0) {
            return res.status(404).send("user id not present in database")

        }
        const cart_items_list:cart_info[]= []
        var total_price:number = 0

        for (const i of result) {
            const cart_items:cart_info=
            {
                product_name: i.product_name,
                product_image: i.product_image,
                product_price: i.product_price,
                product_discount: i.product_discount,
                product_discount_type: i.product_discount_type,
                product_discription: i.product_discription,
                quantity: i.quantity,

            }

            if (i.product_discount_type == "percentage") {
                cart_items["total_amount"] = (i.product_price - (i.product_price * (i.product_discount / 100))) * i.quantity
                cart_items_list.push(cart_items)
                total_price += (i.product_price - (i.product_price * (i.product_discount / 100))) * i.quantity

            }
            else{
                cart_items["total_amount"] = i.product_price - i.product_discount
                cart_items_list.push(cart_items)
                total_price += i.product_price - i.product_discount

            }
            
        }

        res.status(200).send({ user_name: result[0].user_name, total_product: cart_items_list.length, product_list: cart_items_list, total_amount: total_price })

    }

    )
}

)


cart_rout.post("/post", (req:Request,res:Response) => {
    const data = req.body;
    const keyList:(string)[]= ["user_id", "product_id", "quantity"]
    const default_key:(string)[] = []
    if(typeof data.user_id=="string" || typeof data.product_id=="string" || typeof data.quantity=="string")
    {
        return res.status(400).send({error:"invalid syntax"})
    }

    for (const item of Object.keys(data)) {
        if (keyList.includes(item)) {
            continue
        }
        default_key.push(item)
    }
    if (default_key.length == 0) {
       const qry = "INSERT INTO cart(user_id,product_id,quantity) values(?,?,?)";
        cart_cN.query(qry, [data.user_id, data.product_id, data.quantity], (err:any, result:any) => {

            if (err) {
                return res.status(400).send(err.sqlMessage)
            }
            res.status(200).send("data netry is success full")
        }

        )
    }
    else{res.status(422).send({ "not exist": default_key })}

    

})


cart_rout.put("/put/:cart_id", (req:Request,res:Response) => {  // cart_id,product_id,quantity

    cart_cN.query("select * from cart where cart_id=?",req.params.cart_id,(err:any,result:any)=>
    {
        if(err)
        {
            return res.status(400).send(err.sqlMessage)
        }
        else{
            if(result.length==0)
            {
                res.status(422).send("this cart_id not present in database")
            }
            else{
                

                const data = req.body;
                const keyList:(string)[] = ["user_id", "product_id", "quantity"]
                const default_key:(string)[]= []
                if(typeof data.user_id=="string" || typeof data.product_id=="string" || typeof data.quantity=="string")
                {
                    return res.status(400).send({error:"invalid syntax"})
                }
            
            
                for (const item of Object.keys(data)) {
                    if (keyList.includes(item)) {
                        continue;
                    }
                    default_key.push(item)
            
                }
            
            
                if (default_key.length == 0) {
                    const qry:string = "update cart set  user_id=?, product_id=?,quantity=? where cart_id=?";
                    cart_cN.query(qry, [data.user_id, data.product_id, data.quantity, req.params.cart_id], (err:any, result:any) => {
            
                        if (err) {
                            return res.status(400).send(err.sqlMessage)
                        }
            
                        res.status(200).send("Data will be updated")
            
                    }
            
                    )
            
            
                }
                else{

                    res.status(400).send({ "not exist": default_key })
                }
            
                

            }
 
        
        }
    })

   

})

cart_rout.delete("/delete/:cart_id", (req:Request,res:Response) => {

    const qry = "select * from cart where cart_id=?";
    cart_cN.query(qry,req.params.cart_id, (err:any, result:any) => {
        if (err) {
            return res.status(400).send(err.sqlMessage)
        }
        if(result.length==0)
        {
            res.status(400).send("this cart_id note present in database")
        }

        else{
            cart_cN.query("delete from cart where cart_id=?",req.params.cart_id,(err:any,result:any)=>
            {
                if(err)
                {
                    return res.status(400).send(err.sqlMessage)
                }
                return res.status(200).send("data will be deleted")
            })


        }


        
    })



})



module.exports = cart_rout;