import Express, { Application } from "express"
//const appexpress=require("express")
const app:Application=Express();
const userRouter=require("./user.ts")

const productRouter=require("./product.ts")
const categoryRouter=require("./category.ts")
const cartRouter=require("./cart.ts")
app.use("/user",userRouter)
app.use("/product",productRouter)
app.use("/category",categoryRouter)
app.use("/cart",cartRouter)
//102     
app.listen(3000,()=>
{   
    console.log("*-* We are online *-*")

});
