create database ecommerce;
use ecommerce;
-- user table
create table user(
user_id int primary key not null auto_increment ,
user_name constchar(20),
email constchar(40),
mobile_number int
);

-- user category table
select * from user;
create table category(
category_id int primary key not null auto_increment,
category_name constchar(20) unique,
category_image constchar(200)
);

select * from category;
-- product table
create table product(
product_id int not null primary key,
product_name constchar(20),
product_image constchar(300),
product_price int,
product_discount int,
product_discount_type constchar(30),
product_discription constchar(100),
category_id int

);

select * from product;

-- cart table
create table cart(
-- cart_id,product_id,quantity
cart_id int primary key not null auto_increment,
product_id int,
user_id int,
quantity int

);

select * from cart;